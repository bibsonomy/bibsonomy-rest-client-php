<?php
/**
 *
 *  Copyright (C) 2006 - 2013 Knowledge & Data Engineering Group,
 *                            University of Kassel, Germany
 *                            http://www.kde.cs.uni-kassel.de/
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

require_once 'RESTConfig.php';
require_once 'queries/AbstractQuery.php';
require_once 'exceptions/BibsonomyException.php';
require_once 'exceptions/IllegalArgumentException.php';

/**
 * Use this Class to receive an ordered list of all posts.
 * 
 * @author Sebastian Böttger <boettger@cs.uni-kassel.de>
 */

class GetPostDetailsQuery extends AbstractQuery {


    private $start;
    private $end;
    private $order;
    private $search;
    private $resourceType;
    private $tags;
    private $grouping = "ALL";
    private $groupingValue;
    private $resourceHash;

    
    /**
     * Gets bibsonomy's posts list.
     * 
     * @param start
     *            start of the list
     * @param end
     *            end of the list
     */
    public function __construct($apiHostUrl, AuthenticationAccessor $accessor, $username, $resourceHash = null) {

        parent::__construct($apiHostUrl, $accessor, $username);
        
        $this->resourceHash = $resourceHash;
    }
    
    
    /**
     * @param userName
     *            the userName to set
     */
    public function setUserName($username) {
            $this->username = $username;
    }

    /**
     * @param string resourceHash
     *            The resourceHash to set.
     */
    public function setResourceHash($resourceHash) {
            $this->resourceHash = $resourceHash;
    }

    protected function doExecute() {
        
        if(empty($this->resourceHash)) {
            throw new IllegalArgumentException("no resource hash given");
        }
        
        $this->query();

        $client = $this->accessor->getClient();
        
        $client->setMethod(Zend_Http_Client::GET);
        $client->setHeaders(Zend_Http_Client::CONTENT_TYPE, 'application/json;charset=UTF-8');
        
        $client->setUri(
            Zend_Uri_Http::fromString($this->url)
        );
        
        $this->response = $client->request();
    }
    
    /**
     * builds request url
     * @return void
     */
    protected function query() {
        
        $this->url =  $this->apiHostUrl . "/" . RESTConfig::USERS_URL . "/" . $this->username . "/" . RESTConfig::POSTS_URL . "/" . $this->resourceHash;
    }

}
?>