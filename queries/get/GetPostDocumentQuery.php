<?php
/**
 *
 *  BibSonomy-Rest-Client - The REST-client.
 *
 *  Copyright (C) 2006 - 2011 Knowledge & Data Engineering Group,
 *                            University of Kassel, Germany
 *                            http://www.kde.cs.uni-kassel.de/
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

require_once __DIR__.'/../AbstractQuery.php';

/**
 * Downloads a document for a specific post.
 * 
 * @author Sebastian Böttger <boettger@cs.uni-kassel.de>
 * 
 */
class GetPostDocumentQuery extends AbstractQuery {


    private $resourceHash;
        
    private $fileName;

	
    /**
     * 
     * @param string $apiHostUrl
     * @param AuthenticationAccessor $accessor
     * @param string $username
     * @param string $resourceHash
     * @param string $fileName
     * @param boolean $performDownload
     * @throws IllegalArgumentException
     */
    public function __construct($apiHostUrl, AuthenticationAccessor $accessor, $username, $resourceHash, $fileName, $peformDownload = true) {

        parent::__construct($apiHostUrl, $accessor, $username);
                
        $this->resourceHash = $resourceHash;
        
        $this->fileName = $fileName;
        
        $this->performDownload = $peformDownload;
    }
        

    
    protected function doExecute() {
        
        $client = $this->accessor->getClient();
        
        $url = $this->apiHostUrl . "/" . RESTConfig::USERS_URL . "/" . $this->username . "/posts/" . $this->resourceHash . "/documents/" . $this->fileName;
        
        $client->setUri($url);
        
        $client->setStream(); // will use temp file
        $this->response = $client->request('GET');
        
        if($this->performDownload) {
            $this->performFileDownload($this->response);
            unlink($this->response->getStreamName());
            die();
        }
        
        unlink($this->response->getStreamName());
    }
        
    public function performFileDownload($streamResponse) {
        $fp = fopen($streamResponse->getStreamName(), 'r');
        
        header("Content-type: application/octet-stream;charset=UTF-8");
        header('Content-Disposition: attachment; filename="'.$this->fileName.'"');
        
        while(!feof($fp)) {
            print(fread($fp, 8192)); 
        } 
    }
}
