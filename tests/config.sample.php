<?php


$path = "/full/path/to/BibSonomyAPI";

set_include_path(get_include_path() . PATH_SEPARATOR . $path);

/*
 * Insert Host, UserId, apiKey of the user and the IntraHash + InterHash of 
 * publication before running the PHPUnit tests.
 * 
 */

$host       = "http://www.bibsonomy.org/api";

$user       = "xxxx";
$apiKey     = "xxxxxxxxxx";

//IntraHash of a publication for testing
$resourceIntraHashPublication = "xxxxxx";

// InterHash of a publication for testing
$resourceInterHashPublication = "xxxxxx";

// File name of a document of this publication
$fileName = "test.pdf";

?>
